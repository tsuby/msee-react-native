
import React, {Component} from 'react';
import {AppRegistry} from 'react-native';
import {createStore, applyMiddleware, combineReducers} from 'redux';
import createLogger from 'redux-logger';
import thunk from 'redux-thunk';
import {recordingReducer} from './src/recording';
import {authReducer} from './src/auth';
import {Router} from './src/Router'

const rootReducer = combineReducers({recording: recordingReducer, auth: authReducer});
const store = createStore(rootReducer, applyMiddleware(thunk, createLogger({colors: {}})));
// const store = createStore(rootReducer, applyMiddleware(thunk));

export default class mseereactnative extends Component {
    render() {
        return (
            <Router store={store}/>
        );
    }
}


AppRegistry.registerComponent('mseereactnative', () => mseereactnative);
