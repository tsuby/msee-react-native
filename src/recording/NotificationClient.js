const io = require('socket.io-client');
import {serverUrl} from '../core/api';
import {getLogger} from '../core/utils';
import {recordingCreated, recordingUpdated, recordingDeleted} from './service';

window.navigator.userAgent = 'ReactNative';

const log = getLogger('NotificationClient');

const RECORDING_CREATED = 'recording/created';
const RECORDING_UPDATED = 'recording/updated';
const RECORDING_DELETED = 'recording/deleted';

export class NotificationClient {
  constructor(store) {
    this.store = store;
  }

  connect() {
    log(`connect...`);
    const store = this.store;
    const auth = store.getState().auth;
    this.socket = io(auth.server.url, {transports: ['websocket']});
    const socket = this.socket;
    socket.on('connect', () => {
      log('connected');
      socket
        .emit('authenticate', {token: auth.token})
        .on('authenticated', () => log(`authenticated`))
        .on('unauthorized', (msg) => log(`unauthorized: ${JSON.stringify(msg.data)}`))
    });
    socket.on(RECORDING_CREATED, (recording) => {
      log(RECORDING_CREATED);
      store.dispatch(recordingCreated(recording));
    });
    socket.on(RECORDING_UPDATED, (recording) => {
      log(RECORDING_UPDATED);
      store.dispatch(recordingUpdated(recording))
    });
    socket.on(RECORDING_DELETED, (recording) => {
      log(RECORDING_DELETED);
      store.dispatch(recordingDeleted(recording))
    });
  };

  disconnect() {
    log(`disconnect`);
    this.socket.disconnect();
  }
}