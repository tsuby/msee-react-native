import {action, getLogger, errorPayload} from '../core/utils';
import {search, save} from './resource';

const log = getLogger('recording/service');

// Loading recordings
const LOAD_RECORDINGS_STARTED = 'recording/loadStarted';
const LOAD_RECORDINGS_SUCCEEDED = 'recording/loadSucceeded';
const LOAD_RECORDINGS_FAILED = 'recording/loadFailed';
const CANCEL_LOAD_RECORDINGS = 'recording/cancelLoad';

// Saving recordings
const SAVE_RECORDING_STARTED = 'recording/saveStarted';
const SAVE_RECORDING_SUCCEEDED = 'recording/saveSucceeded';
const SAVE_RECORDING_FAILED = 'recording/saveFailed';
const CANCEL_SAVE_RECORDING = 'recording/cancelSave';

// Recording notifications
const RECORDING_DELETED = 'recording/deleted';

export const loadRecordings = () => async(dispatch, getState) => {
  log(`loadRecordings...`);
  const state = getState();
  const recordingState = state.recording;
  try {
    dispatch(action(LOAD_RECORDINGS_STARTED));
    const recordings = await search(state.auth.server, state.auth.token)
    log(`loadRecordings succeeded`);
    if (!recordingState.isLoadingCancelled) {
      dispatch(action(LOAD_RECORDINGS_SUCCEEDED, recordings));
    }
  } catch(err) {
    log(`loadRecordings failed`);
    if (!recordingState.isLoadingCancelled) {
      dispatch(action(LOAD_RECORDINGS_FAILED, errorPayload(err)));
    }
  }
};

export const cancelLoadRecordings = () => action(CANCEL_LOAD_RECORDINGS);

export const saveRecording = (recording) => async(dispatch, getState) => {
  log(`saveRecording...`);
  const state = getState();
  const recordingState = state.recording;
  try {
    dispatch(action(SAVE_RECORDING_STARTED));
    const savedRecording = await save(state.auth.server, state.auth.token, recording)
    log(`saveRecording succeeded`);
    if (!recordingState.isSavingCancelled) {
      dispatch(action(SAVE_RECORDING_SUCCEEDED, savedRecording));
    }
  } catch(err) {
    log(`saveRecording failed`);
    if (!recordingState.isSavingCancelled) {
      dispatch(action(SAVE_RECORDING_FAILED, errorPayload(err)));
    }
  }
};

export const cancelSaveRecording = () => action(CANCEL_SAVE_RECORDING);

export const recordingCreated = (createdRecording) => action(SAVE_RECORDING_SUCCEEDED, createdRecording);
export const recordingUpdated = (updatedRecording) => action(SAVE_RECORDING_SUCCEEDED, updatedRecording);
export const recordingDeleted = (deletedRecording) => action(RECORDING_DELETED, deletedRecording);

export const recordingReducer = (state = {items: [], isLoading: false, isSaving: false}, action) => { //newState (new object)
  let items, index;
  switch (action.type) {
    // Loading
    case LOAD_RECORDINGS_STARTED:
      return {...state, isLoading: true, isLoadingCancelled: false, issue: null};
    case LOAD_RECORDINGS_SUCCEEDED:
      return {...state, items: action.payload, isLoading: false};
    case LOAD_RECORDINGS_FAILED:
      return {...state, issue: action.payload.issue, isLoading: false};
    case CANCEL_LOAD_RECORDINGS:
      return {...state, isLoading: false, isLoadingCancelled: true};
    // Saving
    case SAVE_RECORDING_STARTED:
      return {...state, isSaving: true, isSavingCancelled: false, issue: null};
    case SAVE_RECORDING_SUCCEEDED:
      items = [...state.items];
      index = items.findIndex((i) => i._id == action.payload._id);
      if (index != -1) {
        items.splice(index, 1, action.payload);
      } else {
        items.push(action.payload);
      }
      return {...state, items, isSaving: false};
    case SAVE_RECORDING_FAILED:
      return {...state, issue: action.payload.issue, isSaving: false};
    case CANCEL_SAVE_RECORDING:
      return {...state, isSaving: false, isSavingCancelled: true};
    // Notifications
    case RECORDING_DELETED:
      items = [...state.items];
      const deletedRecording = action.payload;
      index = state.items.findIndex((recording) => recording._id == deletedRecording._id);
      if (index != -1) {
        items.splice(index, 1);
        return {...state, items};
      }
      return state;
    default:
      return state;
  }
};