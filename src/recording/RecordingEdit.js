import React, {Component} from 'react';
import {Text, View, TextInput, ActivityIndicator} from 'react-native';
import {saveRecording, cancelSaveRecording} from './service';
import {registerRightAction, issueToText, getLogger} from '../core/utils';
import styles from '../core/styles';

const log = getLogger('RecordingEdit');
const RECORDING_EDIT_ROUTE = 'recording/edit';

export class RecordingEdit extends Component {
  static get routeName() {
    return RECORDING_EDIT_ROUTE;
  }

  static get route() {
    return {name: RECORDING_EDIT_ROUTE, title: 'Recording Edit', rightText: 'Save'};
  }

  constructor(props) {
    log('constructor');
    super(props);
    this.store = this.props.store;
    const nav = this.props.navigator;
    this.navigator = nav;
    const currentRoutes = nav.getCurrentRoutes();
    const currentRoute = currentRoutes[currentRoutes.length - 1];
    if (currentRoute.data) {
      this.state = {recording: {...currentRoute.data}, isSaving: false};
    } else {
      this.state = {recording: {text: ''}, isSaving: false};
    }
    registerRightAction(nav, this.onSave.bind(this));
  }

  render() {
    log('render');
    const state = this.state;
    let message = issueToText(state.issue);
    return (
      <View style={styles.content}>
        { state.isSaving &&
        <ActivityIndicator animating={true} style={styles.activityIndicator} size="large"/>
        }
        <Text>Text</Text>
        <TextInput value={state.recording.text} onChangeText={(text) => this.updateRecordingText(text)}></TextInput>
        {message && <Text>{message}</Text>}
      </View>
    );
  }

  componentDidMount() {
    log('componentDidMount');
    this._isMounted = true;
    const store = this.props.store;
    this.unsubscribe = store.subscribe(() => {
      log('setState');
      const state = this.state;
      const recordingState = store.getState().recording;
      this.setState({...state, issue: recordingState.issue});
    });
  }

  componentWillUnmount() {
    log('componentWillUnmount');
    this._isMounted = false;
    this.unsubscribe();
    if (this.state.isLoading) {
      this.store.dispatch(cancelSaveRecording());
    }
  }

  updateRecordingText(text) {
    let newState = {...this.state};
    newState.recording.text = text;
    this.setState(newState);
  }

  onSave() {
    log('onSave');
    this.store.dispatch(saveRecording(this.state.recording)).then(() => {
      log('onRecordingSaved');
      if (!this.state.issue) {
        this.navigator.pop();
      }
    });
  }
}