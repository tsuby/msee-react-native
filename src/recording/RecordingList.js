import React, {Component} from 'react';
import {ListView, Text, View, StatusBar, ActivityIndicator} from 'react-native';
import {RecordingEdit} from './RecordingEdit';
import {RecordingView} from './RecordingView';
import {loadRecordings, cancelLoadRecordings} from './service';
import {registerRightAction, getLogger, issueToText} from '../core/utils';
import styles from '../core/styles';

const log = getLogger('RecordingList');
const RECORDING_LIST_ROUTE = 'recording/list';

export class RecordingList extends Component {
  static get routeName() {
    return RECORDING_LIST_ROUTE;
  }

  static get route() {
    return {name: RECORDING_LIST_ROUTE, title: 'Recording List', rightText: 'New'};
  }

  constructor(props) {
    super(props);
    log('constructor');
    this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1.id !== r2.id});
    this.store = this.props.store;
    const recordingState = this.store.getState().recording;
    this.state = {isLoading: recordingState.isLoading, dataSource: this.ds.cloneWithRows(recordingState.items)};
    registerRightAction(this.props.navigator, this.onNewRecording.bind(this));
  }

  render() {
    log('render');
    let message = issueToText(this.state.issue);
    return (
      <View style={styles.content}>
        { this.state.isLoading &&
        <ActivityIndicator animating={true} style={styles.activityIndicator} size="large"/>
        }
        {message && <Text>{message}</Text>}
        <ListView
          dataSource={this.state.dataSource}
          enableEmptySections={true}
          renderRow={recording => (<RecordingView recording={recording} onPress={(recording) => this.onRecordingPress(recording)}/>)}/>
      </View>
    );
  }

  onNewRecording() {
    log('onNewRecording');
    this.props.navigator.push({...RecordingEdit.route});
  }

  onRecordingPress(recording) {
    log('onRecordingPress');
    this.props.navigator.push({...RecordingEdit.route, data: recording});
  }

  componentDidMount() {
    log('componentDidMount');
    this._isMounted = true;
    const store = this.store;
    this.unsubscribe = store.subscribe(() => {
      log('setState');
      const recordingState = store.getState().recording;
      this.setState({dataSource: this.ds.cloneWithRows(recordingState.items), isLoading: recordingState.isLoading});
    });
    store.dispatch(loadRecordings());
  }

  componentWillUnmount() {
    log('componentWillUnmount');
    this._isMounted = false;
    this.unsubscribe();
    if (this.state.isLoading) {
      this.store.dispatch(cancelLoadRecordings());
    }
  }
}
